<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon as Carbon;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        if (env('DB_CONNECTION') == 'mysql')
        {
            DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        }

        if (env('DB_CONNECTION') == 'mysql')
        {
            DB::table('users')->truncate();
        }

        else
        {
            //For PostgreSQL or anything else
            DB::statement('TRUNCATE TABLE ' . 'users' . ' CASCADE');
        }

        //Add the master administrator, user id of 1
        $users = [
            [
                'name'              => 'Administrador',
                'idrol'           => '1',
                'email'             => 'ingcarlos@unicauca.edu.co',
                'password'          => bcrypt('carlitos'),
                'remember_token'    => str_random(100),
                'created_at'        => Carbon::now(),
                'updated_at'        => Carbon::now(),
            ],
            [
                'name'              => 'Docente Juan Pérez',
                'idrol'           => '2',
                'email'             => 'docente@unicauca.edu.co',
                'password'          => bcrypt('docente'),
                'remember_token'    => str_random(100),
                'created_at'        => Carbon::now(),
                'updated_at'        => Carbon::now(),
            ],
            [
                'name'              => 'Estudiante Pepito Pérez',
                'idrol'           => '3',
                'email'             => 'estudiante@unicauca.edu.co',
                'password'          => bcrypt('estudiante'),
                'remember_token'    => str_random(100),
                'created_at'        => Carbon::now(),
                'updated_at'        => Carbon::now(),
            ],

        ];

        DB::table('users')->insert($users);

        if (env('DB_CONNECTION') == 'mysql')
        {
            DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        }

    }
}
