<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon as Carbon;

class MateriasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
     public function run()
     {

         if (env('DB_CONNECTION') == 'mysql')
         {
             DB::statement('SET FOREIGN_KEY_CHECKS=0;');
         }

         if (env('DB_CONNECTION') == 'mysql')
         {
             DB::table('materias')->truncate();
         }

         else
         {
             //For PostgreSQL or anything else
             DB::statement('TRUNCATE TABLE ' . 'materias' . ' CASCADE');
         }

         //Add the master administrator, user id of 1
         $users = [
             [
                 'name'              => 'Cálculo 1',
                 'facultad'          => 'Facultad de Electrónica',
                 'created_at'        => Carbon::now(),
                 'updated_at'        => Carbon::now(),
             ],

         ];

         DB::table('materias')->insert($users);

         if (env('DB_CONNECTION') == 'mysql')
         {
             DB::statement('SET FOREIGN_KEY_CHECKS=1;');
         }

     }
}
