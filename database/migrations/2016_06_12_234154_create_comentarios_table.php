<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateComentariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('comentarios', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_materia');
            $table->integer('id_docente');
            $table->string('comentario');
            $table->enum('polaridad',['positive','negative','neutral']);
            $table->enum('caracteristica',['puntualidad','claridad','organizacion','evaluacion','relaciones']);
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
