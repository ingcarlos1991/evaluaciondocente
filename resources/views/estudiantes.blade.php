@extends('layouts.dashboard')
@section('page_heading','Estudiantes')

@section('section')
</div>
<div class="form-group">
			
			    <a href="{{url('auth/registrar_estudiante')}}" class="btn btn-primary" > <span class="fa fa-plus"></span> Agregar estudiante</a>
			
</div>	
<div class="row">
	<div class="col-sm-12">
		@section ('cotable_panel_title','Estudiantes')
		@section ('cotable_panel_body')
		
<div class="table-responsive">
		<table class="table table-bordered table-hover table-striped ">
			<thead>
				<tr>
					<th>Nombre</th>
					<th>Correo</th>
					<th>Acción</th>
					
					
					
				</tr>
			</thead>
			<tbody>
				 @foreach($estudiantes as $estudiante)

				<tr >
					<td>{{$estudiante->name}}</td>
					<td>{{$estudiante->email}}</td>
					<td><a href="{{url('materias',$estudiante->id)}}" class="btn btn-info btn-xs"><span class="fa fa-chevron-right"></span>   Materias</a>
						<a href="#" class="btn btn-warning btn-xs"><span class="fa fa-chevron-right"></span>   Editar</a></td>
					
				</tr>
				@endforeach
				
			</tbody>
		</table>	
		@endsection
		@include('widgets.panel', array('header'=>true, 'as'=>'cotable'))
	</div>
</div>
</div>
@stop