@extends ('layouts.docente')
@section('page_heading','Mis reportes')
@section('section')

	<div class="container-fluid">
	<div class="row">
		<div class="col-sm-12 col-md-6 col-lg-6">
			<script src="{{ asset("assets/scripts/Chart.min.js") }}" type="text/javascript"></script>
			<div class="row">
				<div class="col-sm-12">
				 <canvas id="canvas" height="450" width="600"></canvas>

			</div>
			</div>


		</div>
		<div class="col-sm-12 col-md-6 col-lg-6">
			@section ('cotable_panel_title','Comentarios')
			@section ('cotable_panel_body')
			<div class="table-responsive">
					<table class="table table-bordered table-hover table-striped ">
						<thead>
							<tr>
								<th>Comentario</th>
								<th>Polaridad</th>


							</tr>
						</thead>
						<tbody>
							 @foreach($comentarios as $comentario)

							<tr >
								<td>{{$comentario->comentario}}</td>
								<td>{{$comentario->polaridad}}</td>

							</tr>
							@endforeach

						</tbody>
					</table>

				</div>
				@endsection
				@include('widgets.panel', array('header'=>true, 'as'=>'cotable'))
	</div>
	</div>




</div>
<script type="text/javascript">
var pieData = [
							 {
									value: {{$porcentaje_positivo}},
									label: 'Positivo',
									color: '#30AF87'
							 },
							 {
									value: {{$porcentaje_negativo}},
									label: 'Negativo',
									color: '#A11111'
							 },
							 {
									value: {{$porcentaje_neutro}},
									label: 'Neutral',
									color: '#1148A1'
							 }
						];

							var myLineChart = new Chart(document.getElementById("canvas").getContext("2d")).Pie(pieData);


</script>

@stop
