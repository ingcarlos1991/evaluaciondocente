<!DOCTYPE html>
<html >
  <head>
    <meta charset="UTF-8">
    <title>Evaluación docente | Iniciar sesión</title>
    
 <link href="{{ asset("/login/img/Unicauca.ico") }}" type="image/x-icon" rel="shortcut icon" />   
<!--<link rel="stylesheet" href="css/reset.css">-->
<link rel="stylesheet" href="{{ asset("/login/css/reset.css") }}">

<link rel='stylesheet prefetch' href='http://fonts.googleapis.com/css?family=Roboto:400,100,300,500,700,900'>
<link rel='stylesheet prefetch' href='http://fonts.googleapis.com/css?family=Montserrat:400,700'>
<link rel='stylesheet prefetch' href='https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css'>

        <link rel="stylesheet" href="{{ asset("/login/css/style.css") }}">
        
        


    
    
    
  </head>

  <body>
 
<div class="container">
  <div class="info">
    <h1></h1>
  </div>
</div>
<div class="form">
<h5>Universidad del Cauca</h5>
<br>
<h1>Sistema de evaluación docente</h1>

  <div class="thumbnail"><img src="{{asset("/login/img/Unicauca.png")}}"/></div>
  <form class="login-form" role="form" method="POST" action="{{url('auth/login')}}">
     <input type="hidden" name="_token" value="{{ csrf_token() }}">

    <input type="text" name ="email" placeholder="@if($errors->first('email')) ******Datos incorrectos****** @else Correo Institucional* @endif" required/>
    
    <input type="password" name = "password" placeholder="@if($errors->first('password')) ******Datos incorrectos****** @else Contraseña* @endif" required/>
    
    <button type="submit">Iniciar sesión</button>
   
  </form>
</div>

    <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>

        <script src="js/index.js"></script>
        <script src="{{ asset("/login/js/index.js") }}" type="text/javascript"></script>


    
    
    
  </body>
</html>
