@extends('layouts.dashboard')
@section('page_heading','Estudiante - Materias')

@section('section')
</div>
<div class="form-group">
			
			    <a data-toggle="modal" data-target="#ModalAgregar" class="btn btn-primary" > <span class="fa fa-plus"></span> Agregar materia</a>
			
</div>	
<div class="row">
	<div class="col-sm-12">
		@section ('cotable_panel_title','Estudiante')
		@section ('cotable_panel_body')
		<h3 align="center">Estudiante: {{$nombre->name}}</h3>
<form   method="POST" action="{{url('estudiante_materias')}}">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<input type="hidden" name="id_estudiante" value="{{ $nombre->id }}">
<div class="table-responsive">
		<table class="table table-bordered table-hover table-striped ">
			<thead>
				<tr>
					<th>Nombre</th>
					<th>Facultad</th>
					<th>Docente</th>
					<th>Acción</th>
					
					
					
				</tr>
			</thead>
			<tbody>
				 @foreach($materias_estudiante as $materia_estudiante)

				<tr >
					<td>{{$materia_estudiante->materia->name}}</td>
					<td>{{$materia_estudiante->materia->facultad}}</td>
					@if($materia_estudiante->materia->docente == NULL)
					<td>Sin asociar docente</td>
					@else
					<td>{{$materia_estudiante->materia->docente}}</td>
					@endif
					
					<td>
						<a href="#" class="btn btn-danger btn-xs"><span class="fa fa-chevron-right"></span>   Eliminar</a></td>
					
				</tr>
				@endforeach
				
			</tbody>
		</table>	
		@endsection
		@include('widgets.panel', array('header'=>true, 'as'=>'cotable'))
	</div>
</div>
</div>

<div class="modal fade" id="ModalAgregar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 align ="center" class="modal-title" id="myModalLabel">Agregar materia</h4>
      </div>
      <div class="modal-body" align="center">
      	
        <div class="form-group">
							
							
							<select class="form-control" name="id_materia" required>
								 @foreach($materias as $materia)

			                    <option value="{{$materia->id}}">{{$materia->name}} - {{$materia->facultad}}</option>
			                    
			                    @endforeach
			                   
			                  </select>

							
							
		</div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
        <button type="submit" class="btn btn-success" >Aceptar</button>
    </form>
      </div>
    </div>
  </div>
</div>
@stop