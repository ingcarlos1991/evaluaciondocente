@extends('layouts.dashboard')
@section('page_heading','Agregar materia')
@section('section')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading">Formulario de registro</div>
				<div class="panel-body">
					@if (count($errors) > 0)
						<div class="alert alert-danger">
							<strong>!Cuidado!</strong> Hay problemas con las entradas.<br><br>
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif

					<form class="form-horizontal" role="form" method="POST" action="{{url('agregar_materia')}}">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">

						<div class="form-group">
							<label class="col-md-4 control-label">Nombre</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="name" value="{{ old('name') }}">

							</div>
							
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label">Facultad</label>
							<div class="col-md-6">
								<select class="form-control" name="facultad" required>
			                    <option value="Facultad de Civil">Facultad de Civil</option>
			                    <option value="Facultad de Electrónica">Facultad de Electrónica</option>
			                    <option value="Facultad de Contables">Facultad de Contables</option>
			                    <option value="Facultad de Educación">Facultad de Educación</option>
			                    <option value="Facultad de Derecho">Facultad de Derecho</option>
			                   
			                  </select>

							</div>
							
						</div>
						
						

						

						

						<div class="form-group">
							<div class="col-md-6 col-md-offset-4">
								<button type="submit" class="btn btn-primary">
									Agregar
								</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
