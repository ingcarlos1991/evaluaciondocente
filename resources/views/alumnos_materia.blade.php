@extends('layouts.dashboard')
@section('page_heading','Estudiantes - Materia')

@section('section')
</div>
	
<div class="row">
	<div class="col-sm-12">
		@section ('cotable_panel_title','Materia')
		@section ('cotable_panel_body')
		<h3 align="center">Materia: {{$materia->name}}</h3>
<form   method="POST" action="#">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						
<div class="table-responsive">
		<table class="table table-bordered table-hover table-striped ">
			<thead>
				<tr>
					<th>Nombre</th>
					<th>Correo</th>
					
					<th>Acción</th>
					
					
					
				</tr>
			</thead>
			<tbody>
				 @foreach($alumnos as $alumno)

				<tr >
					<td>{{$alumno->estudiante->name}}</td>
					<td>{{$alumno->estudiante->email}}</td>
					<td>
						<a href="#" class="btn btn-danger btn-xs"><span class="fa fa-chevron-right"></span>   Eliminar</a></td>
					
				</tr>
				@endforeach
				
			</tbody>
		</table>	
		@endsection
		@include('widgets.panel', array('header'=>true, 'as'=>'cotable'))
	</div>
</div>
</div>


@stop