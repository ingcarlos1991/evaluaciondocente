@extends('layouts.plane')

@section('body')
 <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="{{ url ('home_docente') }}">Docente | Unicauca</a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
               
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#" >
                        <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="#"><i class="fa fa-user fa-fw"></i> Perfil de usuario</a>
                        </li>
                        
                        <li class="divider"></li>
                        <li><a href="{{ url ('auth/logout') }}"><i class="fa fa-sign-out fa-fw"></i> Cerrar sesión</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                       
                        <li {{ (Request::is('*home_docente') ? 'class="active"' : '') }}>
                            <a href="{{ url ('home_docente') }}"><i class="fa fa-dashboard fa-fw"></i> Inicio</a>
                        </li>
                        <li {{ (Request::is('*mis_reportes') ? 'class="active"' : '') }}>
                            <a href="{{ url ('mis_reportes') }}"><i class="fa fa-bar-chart-o fa-fw"></i> Mis reportes</a>
                            <!-- /.nav-second-level -->
                        </li>
                       
                        
                        
                      
                       
                       
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        <div id="page-wrapper">
			 <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">@yield('page_heading')</h1>
                </div>
                <!-- /.col-lg-12 -->
           </div>
            @if(Session::has('error_status'))

                <div class="alert alert-danger alert-dismissible" align="center" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <h4><i class="icon fa fa-ban"></i> ¡Error!</h4>
                                {{ Session::get('error_status') }}
                </div>

            @endif

            @if(Session::has('success_status'))

            <div class="alert alert-success alert-dismissible" align="center" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h4><i class="icon fa fa-check"></i> ¡Listo!</h4>
                            {{ Session::get('success_status') }}
             </div>

            @endif

            @if(Session::has('warning_status'))

            <div class="alert alert-warning alert-dismissible" align="center" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h4><i class="icon fa fa-warning"></i> ¡Importante!</h4>
                            {{ Session::get('warning_status') }}
             </div>

            @endif
			<div class="row">  
				@yield('section')

            </div>
            <!-- /#page-wrapper -->
        </div>
    </div>
@stop

