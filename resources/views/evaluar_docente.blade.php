@extends('layouts.estudiante')
@section('page_heading','Evaluar docente')

@section('section')

<div class="container-fluid">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading">Seleccione la materia y escriba su comentario</div>
				<div class="panel-body">
					@if (count($errors) > 0)
						<div class="alert alert-danger">
							<strong>!Cuidado!</strong> Hay problemas con las entradas.<br><br>
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif

					<form class="form-horizontal" role="form" method="POST" action="{{url('evaluar_docente')}}">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						

						
							
						
						<div class="form-group">
							<label class="col-md-4 control-label">Materia</label>
							<div class="col-md-6">
								<select class="form-control" name="id_materia" required>
							@foreach($materias as $materia)
			                    <option value="{{$materia->id_materia}}">{{$materia->materia->name}} - {{$materia->materia->docente}}</option>
			                @endforeach
    
			                   
			                  </select>

							</div>
							
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label">Comentario</label>
							<div class="col-md-6">
								<textarea class="form-control" name = "comentario" rows="3"></textarea>
							</div>
							
						</div>

						
						
						

						

						

						<div class="form-group">
							<div class="col-md-6 col-md-offset-4">
								<button type="submit" class="btn btn-primary">
									Enviar
								</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@stop