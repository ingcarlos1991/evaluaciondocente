@extends('layouts.dashboard')
@section('page_heading','Agregar docente')
@section('section')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading">Seleccione la opción</div>
				<div class="panel-body">
					@if (count($errors) > 0)
						<div class="alert alert-danger">
							<strong>!Cuidado!</strong> Hay problemas con las entradas.<br><br>
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif

					<form class="form-horizontal" role="form" method="POST" action="{{url('agregar_profesor')}}">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<input type="hidden" name="id" value="{{ $materia->id }}">

						<div class="form-group">
							<label class="col-md-4 control-label">Nombre</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="name" value="{{ $materia->name }}" disabled>

							</div>
							
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label">Docente</label>
							<div class="col-md-6">
								<select class="form-control" name="docente" required>
							@foreach($profesores as $profesor)
			                    <option value="{{$profesor->name}}">{{$profesor->name}}</option>
			                @endforeach
    
			                   
			                  </select>

							</div>
							
						</div>
						
						

						

						

						<div class="form-group">
							<div class="col-md-6 col-md-offset-4">
								<button type="submit" class="btn btn-primary">
									Agregar
								</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection