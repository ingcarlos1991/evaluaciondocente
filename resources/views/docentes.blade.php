@extends ('layouts.dashboard')

@section ('page_heading','Docentes')

@section('section')
</div>
<div class="form-group">
			
			    <a href="{{url('auth/registrar_docente')}}" class="btn btn-primary" > <span class="fa fa-plus"></span> Agregar docente</a>
			
</div>	
<div class="row">
	<div class="col-sm-12">
		@section ('cotable_panel_title','Estudiantes')
		@section ('cotable_panel_body')
<div class="table-responsive">
		<table class="table table-bordered table-hover table-striped ">
			<thead>
				<tr>
					<th>Nombre</th>
					<th>Correo</th>
					
					
				</tr>
			</thead>
			<tbody>
				 @foreach($docentes as $docente)

				<tr >
					<td>{{$docente->name}}</td>
					<td>{{$docente->email}}</td>
					
				</tr>
				@endforeach
				
			</tbody>
		</table>	
		@endsection
		@include('widgets.panel', array('header'=>true, 'as'=>'cotable'))
	</div>
</div>
</div>
	
@stop
