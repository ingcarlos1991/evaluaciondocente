@extends ('layouts.dashboard')

@section ('page_heading','Materias')

@section('section')
</div>
<div class="form-group">
			
			    <a href="{{url('agregar_materia')}}" class="btn btn-primary" > <span class="fa fa-plus"></span> Agregar materia</a>
			
</div>	
<div class="row">
	<div class="col-sm-12">
		@section ('cotable_panel_title','Materias')
		@section ('cotable_panel_body')
<div class="table-responsive">
		<table class="table table-bordered table-hover table-striped ">
			<thead>
				<tr>
					<th>Nombre</th>
					<th>Facultad</th>
					<th>Docente</th>
					<th>Acción</th>
					
				</tr>
			</thead>
			<tbody>
				 @foreach($materias as $materia)

				<tr >
					<td>{{$materia->name}}</td>
					<td>{{$materia->facultad}}</td>
					@if($materia->docente == NULL)
					<td>Sin asociar docente</td>
					@else
					<td>{{$materia->docente}}</td>
					@endif
					<td>
						@if($materia->docente == NULL)
					<a href="{{url('agregar_profesor',$materia->id)}}" class="btn btn-success btn-xs"><span class="fa fa-plus"></span>   Agregar docente</a>
					@endif
						<a href="{{url('alumnos',$materia->id)}}" class="btn btn-info btn-xs"><span class="fa fa-chevron-right"></span>   Alumnos</a>
						<a href="#" class="btn btn-warning btn-xs"><span class="fa fa-chevron-right"></span>   Editar</a></td>
				</tr>
				@endforeach
				
			</tbody>
		</table>	
		@endsection
		@include('widgets.panel', array('header'=>true, 'as'=>'cotable'))
	</div>
</div>
</div>
</div>
	
@stop
