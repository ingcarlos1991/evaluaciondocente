<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Estudiante_materia extends Model
{
    protected $table = 'estudiantes_materias';

    public function materia()
    {


    	return $this->belongsTo('App\Materia','id_materia');

    }

    public function estudiante()
    {

    	return $this->belongsTo('App\User','id_estudiante');
    }
}
