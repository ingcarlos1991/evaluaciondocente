<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;
use Session;
use Request;


class Administrador
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    protected $auth;
    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }
    public function handle($request, Closure $next)
    {


        if ($this->auth->check()) 
        {
        switch ($this->auth->user()->idrol) {
            case '1':
                //return redirect()->to('home');
                break;
            case '2':

                return redirect()->to('home_docente');
                break;

            case '3':
                return redirect()->to('home_estudiante');
                break;

           

            default:
                return redirect()->to('auth/logout');
                break;
        }
            
        }
        return $next($request);
    }
}
