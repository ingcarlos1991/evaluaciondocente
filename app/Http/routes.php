<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

$router->group(['middleware' => 'guest'], function() {


Route::get('auth/login','Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');


    
});

$router->group(['middleware' => 'auth'], function() {

Route::get('auth/logout', 'Auth\AuthController@getLogout');
	

});

$router->group(['middleware' => 'administrador'], function() {

Route::get('auth/register','Auth\AuthController@getRegister');
Route::post('auth/register','Auth\AuthController@postRegister');

Route::get('auth/registrar_estudiante','Auth\AuthController@getRegistrarEstudiante');
Route::get('auth/registrar_docente','Auth\AuthController@getRegistrarDocente');
Route::get('materias','MateriaController@index');
Route::get('agregar_materia','MateriaController@add');
Route::post('agregar_materia','MateriaController@post_agregar');
Route::get('estudiantes','EstudianteController@index');
Route::get('docentes','DocenteController@index');
Route::get('materias/{id}','EstudianteController@materias');
Route::post('estudiante_materias','EstudianteController@post_materias');
Route::get('agregar_profesor/{id}','MateriaController@agregar_profesor');
Route::post('agregar_profesor','MateriaController@post_agregar_profesor');
Route::get('alumnos/{id}','MateriaController@getAlumnos');
Route::get('home','HomeController@index');
Route::get('reportes','ReporteController@index');
	

});

$router->group(['middleware' => 'estudiante'], function() {

Route::get('home_estudiante','HomeController@index_estudiante');
Route::get('evaluar_docente','EstudianteController@evaluar_docente');
Route::post('evaluar_docente','EstudianteController@evaluar_docente_post');
	

});
$router->group(['middleware' => 'docente'], function() {

Route::get('home_docente','HomeController@index_docente');
Route::get('mis_reportes','ReporteController@mis_reportes');
	

});





