<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Http\Requests;
use Illuminate\Contracts\Auth\Guard;

class DocenteController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
    	$docentes = User::where('idrol','=','2')->get();
    	return View('docentes')->with('docentes',$docentes);
    }
}
