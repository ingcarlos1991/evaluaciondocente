<?php namespace App\Http\Controllers;

class HomeController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	
	

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function __construct()
    {
        $this->middleware('auth');
    }
	public function index()
	{
		return view('home');
	}

	public function index_estudiante()
	{
		return view('home_estudiante');
	}
	public function index_docente()
	{
		return view('home_docente');
	}

}
