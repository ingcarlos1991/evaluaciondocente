<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Materia;
use App\Estudiante_materia;
use App\User;
use App\Http\Requests;
use Validator;

class MateriaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
    	$materias = Materia::get();
    	return View('materias')
    	->with('materias',$materias);
    }
    public function add()
    {
    	return View('agregar_materia');
    }
    public function getAlumnos($id)
    {
        $materia = Materia::find($id);
        $alumnos = Estudiante_materia::where('id_materia','=',$id)->get();
        return View('alumnos_materia')
        ->with('alumnos',$alumnos)
        ->with('materia',$materia);
    }
    public function agregar_profesor($id)
    {

        $profesores = User::where('idrol','=','2')->orderBy('name','ASC')->paginate(15);
        $materia = Materia::find($id);
        return View('agregar_profesor')
        ->with('materia',$materia)
        ->with('profesores',$profesores);

    }

    public function post_agregar_profesor(Request $request)
    {

        $materia = Materia::find($request->id);
        $nombre = $materia->name;
        $materia->docente = $request->docente;
        $materia->save();
        return redirect('materias')->with('success_status','Docente agregado a la materia '.$nombre.' correctamente');

    }


    public function post_agregar(Request $request)
    {
    	
    	 $rules = [
            'name' => 'required|min:3|max:50|regex:/^[1-9a-záéíóúàèìòùäëïöüñ\s]+$/i',
            'facultad' => 'required|min:3|max:50|regex:/^[a-záéíóúàèìòùäëïöüñ\s]+$/i',
            
            //'idrol' => 'required|in:1,2,3,4'
        ];
        
        $messages = [
            'name.required' => 'El campo nombre es requerido',
            'name.min' => 'El mínimo de caracteres permitidos son 3',
            'name.max' => 'El máximo de caracteres permitidos son 20',
            'name.regex' => 'Sólo se aceptan letras',
            'facultad.required' => 'El campo facultad es requerido',
            'facultad.min' => 'El mínimo de caracteres permitidos son 3',
            'facultad.max' => 'El máximo de caracteres permitidos son 20',
            'facultad.regex' => 'Sólo se aceptan letras',
            
            
        ];
        
        $validator = Validator::make($request->all(), $rules, $messages);
        
        if ($validator->fails())
        {
            return redirect("agregar_materia")
            ->withErrors($validator)
            ->withInput();
        }
        else
        {
        			$materia = new Materia;
                    $materia->name = $request->name;
                    $materia->facultad = $request->facultad;
                    $materia->save();
                    return redirect("materias")->with('success_status','Materia registrada correctamente');
        }
    }
}
