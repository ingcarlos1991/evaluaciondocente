<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Contracts\Auth\Guard;
use App\Http\Requests;
use App\User;
use App\Estudiante_materia;
use App\Comentario;
use Auth;

class ReporteController extends Controller
{

	 public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
    	return View('reportes');
    }
    public function mis_reportes()
    {
		  	$variable_chart = 100;
				$estudiantes = User::all();
        $id = Auth::user()->id;
        $docente = User::where('idrol','=','2')->where('id','=',$id)->first();
        $total = Comentario::where('id_docente','=',$docente->id)->count();
				if($total > 0)
				{
					$comentarios = Comentario::where('id_docente','=',$docente->id)->get();
	        $positivos = Comentario::where('id_docente','=',$docente->id)->where('polaridad','=','positive')->count();
	        $negativos = Comentario::where('id_docente','=',$docente->id)->where('polaridad','=','negative')->count();
	        $neutros = Comentario::where('id_docente','=',$docente->id)->where('polaridad','=','neutral')->count();

	        $porcentaje_positivo = ($positivos * 100)/$total;
	        $porcentaje_negativo = ($negativos * 100)/$total;
	        $porcentaje_neutro = ($neutros * 100)/$total;
	        return View('mis_reportes')
	        ->with('porcentaje_positivo',$porcentaje_positivo)
	        ->with('porcentaje_negativo',$porcentaje_negativo)
	        ->with('porcentaje_neutro',$porcentaje_neutro)
					->with('variable_chart',$variable_chart)
					->with('comentarios',$comentarios)
					->with('estudiantes',$estudiantes);
			  }
				else
				{

					$porcentaje_positivo = 0;
					$porcentaje_negativo = 0;
					$porcentaje_neutro = 0;
					return View('mis_reportes')
					->with('porcentaje_positivo',$porcentaje_positivo)
					->with('porcentaje_negativo',$porcentaje_negativo)
					->with('porcentaje_neutro',$porcentaje_neutro)
					->with('variable_chart',$variable_chart);


				}
    }
}
