<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Estudiante_materia;
use App\Materia;
use App\Comentario;
use App\Http\Requests;
use Auth;
use App\Classes\alchemyapi;


class EstudianteController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
    	$estudiantes = User::where('idrol','=','3')->get();
    	return View('estudiantes')->with('estudiantes',$estudiantes);
    }
    public function materias($id)
    {
    	$nombre = User::find($id);
    	$materias = Materia::all();
    	$materias_estudiante = Estudiante_materia::where('id_estudiante','=',$id)->get();

    	return View('estudiante_materias')
    	->with('materias_estudiante',$materias_estudiante)
    	->with('nombre',$nombre)
    	->with('materias',$materias);
    }

    public function post_materias(Request $request)
    {
    	$estudiante_materia = New Estudiante_materia;
    	$estudiante_materia->id_estudiante = $request->id_estudiante;
    	$estudiante_materia->id_materia = $request->id_materia;
    	$estudiante_materia->save();
    	return redirect()->back()->with('success_status','Materia agregada correctamente');
    }


    public function evaluar_docente()
    {
        $id = Auth::user()->id;
        $materias = Estudiante_materia::where('id_estudiante','=',$id)->get();

        return View('evaluar_docente')
        ->with('materias',$materias);
    }

    public function evaluar_docente_post(Request $request)
    {

        $alchemyapi =new  AlchemyAPI('a64be312c5b4925a8da9707d00de74ea403cf8c5');
        $myText = $request->comentario;
        $materia = Materia::find($request->id_materia);
        $docente = User::where('idrol','=','2')->where('name','=',$materia->docente)->first();
        $cadena_de_texto = $myText;
        $cadena_buscada   = 'hp';
        $posicion_coincidencia = strpos($cadena_de_texto, $cadena_buscada);

        //se puede hacer la comparacion con 'false' o 'true' y los comparadores '===' o '!=='
        if ($posicion_coincidencia === false)
        {

            $response2 = $alchemyapi->relations("text", $myText, null);
            $contador = count($response2['relations']);
            for ($i = 0; $i < $contador; $i++)
            {
                $myText = $response2['relations'][$i]['subject']['text'].' '.$response2['relations'][$i]['action']['text'].' '.$response2['relations'][$i]['object']['text'];
                 $response = $alchemyapi->sentiment("text", $myText, null);
                 $language = $response['language'];
                 $status = $response['status'];
                if($status == "OK")
                {
                    if($language == "spanish")
                    {
                        $comentario = New Comentario;
                        $polaridad = $response['docSentiment']['type'];
                        $comentario->id_materia = $request->id_materia;
                        $comentario->comentario = $myText;
                        $comentario->id_docente = $docente->id;
                        $comentario->polaridad = $polaridad;
                        $comentario->save();


                    }
                    else
                    {
                        return redirect()->back()->with('warning_status','Intenta mejorar el comentario, no es muy claro');
                    }

                }
                else
                {
                    return redirect()->back()->with('warning_status','Intenta mejorar el comentario, no es muy claro');
                }
            }
            return redirect("home_estudiante")->with('success_status','Comentario registrado correctamente');

        }
        else
        {
            return redirect()->back()->with('error_status','Tu comentario no ha sido agregado por uso de un
                lenguaje vulgar, si reincides puedes ser bloqueado');

        }







    }
}
