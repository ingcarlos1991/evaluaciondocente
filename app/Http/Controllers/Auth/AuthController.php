<?php
namespace App\Http\Controllers\Auth;

namespace App\Http\Controllers\Auth;

use App\User;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Http\Request;
use Auth;
use Laracasts\Flash\Flash;
use Carbon\Carbon;
use Illuminate\Contracts\Auth\Guard;



class AuthController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Registration & Login Controller
	|--------------------------------------------------------------------------
	|
	| This controller handles the registration of new users, as well as the
	| authentication of existing users. By default, this controller uses
	| a simple trait to add these behaviors. Why don't you explore it?
	|
	*/

	use AuthenticatesAndRegistersUsers;



	public function getRegister()
    {

        return View('auth.register');

    }
    public function getRegistrarEstudiante()
    {

        return View('auth.registrar_estudiante');

    }
    public function getRegistrarDocente()
    {

        return View('auth.registrar_docente');

    }
    public function getLogin()
    {
        return View('auth.login');
    }
    public function postRegister(Request $request)
    {



        $rules = [
            'name' => 'required|min:3|max:20|regex:/^[a-záéíóúàèìòùäëïöüñ\s]+$/i',
            'email' => 'required|email|max:255|unique:users,email',
            'password' => 'required|min:6|max:18|confirmed',
            //'idrol' => 'required|in:1,2,3,4'
        ];

        $messages = [
            'name.required' => 'El campo nombre es requerido',
            'name.min' => 'El mínimo de caracteres permitidos son 3',
            'name.max' => 'El máximo de caracteres permitidos son 20',
            'name.regex' => 'Sólo se aceptan letras',
            'email.required' => 'El campo es correo requerido',
            'email.email' => 'El formato de email es incorrecto',
            'email.max' => 'El máximo de caracteres permitidos son 255',
            'email.unique' => 'El email ya existe',
            'password.required' => 'El campo contraseña es requerido',
            'password.min' => 'El mínimo de caracteres permitidos son 6',
            'password.max' => 'El máximo de caracteres permitidos son 18',
            'password.confirmed' => 'Los passwords no coinciden',

        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails())
        {
            return redirect()->back()
            ->withErrors($validator)
            ->withInput();
        }
        else
        {
        			$rol = $request->idrol;
 					$user = new User;
                    $user->idrol = $request->idrol;
                    $user->name = $request->name;
                    $user->email = $request->email;
                    $user->password = bcrypt($request->password);

                    $user->remember_token = str_random(100);
                    //$user->ip = $ip;
                    //$user->fecha = Carbon::now();
                   // $user->confirm_token = str_random(100);
                    $user->save();
                    if($rol == 2)
                    {
                    	return redirect("docentes")->with('success_status','Docente registrado correctamente');
                    }
                    else
                    {
                    	if( $rol == 3)
                    	{
                    		return redirect("estudiantes")->with('success_status','Estudiante registrado correctamente');
                    	}
                        else
                        {
                            if($rol == 1)
                            {
                                return redirect("home")->with('success_status','Administrador registrado correctamente');
                            }

                        }

                    }


        }

      }



      public function postLogin(Request $request)
    {


        if (Auth::attempt(
             [

                'email' => $request->email,
                'password' => $request->password,
                //'active' => 1

            ]
            , $request->has('remember')
            ))
        {

            return redirect()->intended($this->redirectPath());

        }

        else
        {

            $rules = [
                'email' => 'required|email',
                'password' => 'required',
            ];

            $messages = [
                'email.required' => 'El campo email es requerido',
                'email.email' => 'El formato de email es incorrecto',
                'password.required' => 'El campo password es requerido',
            ];

            $validator = Validator::make($request->all(), $rules, $messages);
           // Flash::error("Error al iniciar sesión");
            return redirect('auth/login')->with('error_status','Datos incorrectos')
            ->withErrors($validator)
            ->withInput();
            //->with('message', 'Error al iniciar sesión');

        }
    }


        public function getLogout()
     {

        auth::logout();
        return redirect('auth/login');
     }
  }
